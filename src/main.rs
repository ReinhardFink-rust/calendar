use std::process::Command;
use crate::latex_file_creator::*;

mod date_processor;
mod latex_file_creator;


fn main() {
    let mut lfc = LaTeXFileCreator::new(
        read_template_from_path(),
        read_year_from_console()
    );
    lfc.process();
    //lfc.print();
    lfc.write();
    Command::new("pdflatex").arg(CALENDAR_TEX).output(). expect("failed to execute process");
    Command::new("rm").arg(CALENDAR_LOG).output().expect("failed to execute process");
    Command::new("rm").arg(CALENDAR_AUX).output().expect("failed to execute process");
    Command::new("evince").arg("Kalender.pdf").output().expect("failed to execute process");
}
