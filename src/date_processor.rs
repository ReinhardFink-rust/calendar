use chrono::{Datelike, NaiveDate};


fn get_second_monday_in_september(year : i32) -> NaiveDate {
    let sep01 = NaiveDate::from_ymd_opt(year, 9, 1).unwrap();
    if sep01.weekday().num_days_from_monday() == 0 {
        NaiveDate::from_ymd_opt(year, 9,  8).unwrap()
    } else {
        NaiveDate::from_ymd_opt(year, 9, 14 - sep01.weekday().num_days_from_monday() + 1).unwrap()
    }
}

fn get_second_monday_in_september_minus_2_weeks(year : i32) -> NaiveDate {
    let mut start = get_second_monday_in_september(year);
    for _ in 0..14 {
        start = start.pred_opt().unwrap();
        //println!("{:?}", start)
    }
    start
}


fn get_second_monday_in_july(year : i32) -> NaiveDate {
    let jul01 = NaiveDate::from_ymd_opt(year, 7, 1).unwrap();
    if jul01.weekday().num_days_from_monday() == 0 {
        NaiveDate::from_ymd_opt(year, 7, 8).unwrap()
    } else {
        NaiveDate::from_ymd_opt(year, 7, 14 - jul01.weekday().num_days_from_monday() + 1).unwrap()
    }
}


pub struct DateProcessor {
    current_day: NaiveDate,
    number_current_day: i32,
    number_of_current_school_week: i32,
    stop_monday: NaiveDate,
}

impl DateProcessor {
    pub fn new(year: i32) -> Self {
        let dp = DateProcessor {
            current_day: get_second_monday_in_september_minus_2_weeks(year),
            number_current_day: 1,
            number_of_current_school_week: -2,
            stop_monday: get_second_monday_in_july(year + 1),
        };
        dp
    }

    pub fn get_string_year(&self) -> String {
        self.current_day.year().to_string()
    }

    pub fn get_string_numer_of_normal_year_week(&self) -> String {
        self.current_day.format("%W").to_string()
    }

    pub fn get_string_numer_of_school_year_week(&self) -> String {
        self.number_of_current_school_week.to_string()
    }

    pub fn get_string_date_day(&self) -> String {
        self.current_day.day().to_string()
    }
    #[allow(dead_code)]
    fn get_string_date_day_per_name(&self) -> String {
        match self.current_day.weekday() {
            chrono::Weekday::Mon => "Montag".to_string(),
            chrono::Weekday::Tue => "Dienstag".to_string(),
            chrono::Weekday::Wed => "Mittwoch".to_string(),
            chrono::Weekday::Thu => "Donnerstag".to_string(),
            chrono::Weekday::Fri => "Freitag".to_string(),
            chrono::Weekday::Sat => "Samstag".to_string(),
            chrono::Weekday::Sun => "Sonntag".to_string(),
        }
    }

    pub fn get_string_date_month(&self) -> String {
        match self.current_day.month() {
            1 => "Jänner".to_string(),
            2 => "Februar".to_string(),
            3 => "März".to_string(),
            4 => "April".to_string(),
            5 => "Mai".to_string(),
            6 => "Juni".to_string(),
            7 => "Juli".to_string(),
            8 => "August".to_string(),
            9 => "September".to_string(),
            10 => "Oktober".to_string(),
            11 => "November".to_string(),
            12 => "Dezember".to_string(),
            _ => "".to_string()
        }
    }

    pub fn has_next(&self) -> bool {
        self.current_day < self.stop_monday
    }

    /// strange counting in next:
    /// school weeks are:
    /// counter: -2 -1 0 1 ...
    /// weeks:   -2 -1 1 2 ...
    pub fn next(&mut self)  {
        self.current_day = self.current_day.succ_opt().unwrap();
        self.number_current_day += 1;
        self.number_of_current_school_week = (self.number_current_day / 7 + 1) - 3;
        if self.number_of_current_school_week >= 0 {
           self.number_of_current_school_week += 1;
        }
    }
}


#[test]
fn test_get_second_monday_in_september() {
    assert_eq!(get_second_monday_in_september(2020), NaiveDate::from_ymd_opt(2020, 9, 14).unwrap());
    assert_eq!(get_second_monday_in_september(2021), NaiveDate::from_ymd_opt(2021, 9, 13).unwrap());
    assert_eq!(get_second_monday_in_september(2022), NaiveDate::from_ymd_opt(2022, 9, 12).unwrap());
    assert_eq!(get_second_monday_in_september(2023), NaiveDate::from_ymd_opt(2023, 9, 11).unwrap());
    assert_eq!(get_second_monday_in_september(2024), NaiveDate::from_ymd_opt(2024, 9, 9).unwrap());
}

#[test]
fn test_get_second_monday_in_july() {
    assert_eq!(get_second_monday_in_july(2020), NaiveDate::from_ymd_opt(2020, 7, 13).unwrap());
    assert_eq!(get_second_monday_in_july(2021), NaiveDate::from_ymd_opt(2021, 7, 12).unwrap());
    assert_eq!(get_second_monday_in_july(2022), NaiveDate::from_ymd_opt(2022, 7, 11).unwrap());
    assert_eq!(get_second_monday_in_july(2023), NaiveDate::from_ymd_opt(2023, 7, 10).unwrap());
    assert_eq!(get_second_monday_in_july(2024), NaiveDate::from_ymd_opt(2024, 7, 8).unwrap());
}

#[test]
fn test_date_processor_string() {
    let mut dp = DateProcessor::new(2021);
    assert_eq!(dp.get_string_year(),"2021");
    assert_eq!(dp.get_string_numer_of_normal_year_week(),"35");
    assert_eq!(dp.get_string_numer_of_school_year_week(),"-2");
    assert_eq!(dp.get_string_date_day(),"30");
    assert_eq!(dp.get_string_date_month(),"August");
    assert_eq!(dp.has_next(),true);
    dp.next();
    assert_eq!(dp.get_string_date_day(),"31");
}

#[test]
fn test_date_processor_next() {
    let mut dp = DateProcessor::new(2021);
    assert_eq!(dp.get_string_year(),"2021");
    assert_eq!(dp.get_string_numer_of_normal_year_week(),"35");
    assert_eq!(dp.get_string_numer_of_school_year_week(),"-2");
    assert_eq!(dp.get_string_date_day(),"30");
    assert_eq!(dp.get_string_date_month(),"August");
    assert_eq!(dp.has_next(),true);
    dp.next();
    assert_eq!(dp.get_string_date_day(),"31");
    dp.next();
    assert_eq!(dp.get_string_date_day(),"1");
    dp.next();
    assert_eq!(dp.get_string_date_day(),"2");
    dp.next();
    assert_eq!(dp.get_string_date_day(),"3");
    dp.next();
    assert_eq!(dp.get_string_date_day(),"4");
    dp.next();
    assert_eq!(dp.get_string_date_day(),"5");
    dp.next();
    assert_eq!(dp.get_string_date_day(),"6");
    assert_eq!(dp.get_string_numer_of_normal_year_week(),"36");
    assert_eq!(dp.get_string_numer_of_school_year_week(),"-1");

    let mut dp = DateProcessor::new(2024);
    assert_eq!(dp.get_string_year(),"2024");
    assert_eq!(dp.get_string_numer_of_normal_year_week(),"35");
    assert_eq!(dp.get_string_numer_of_school_year_week(),"-2");
    assert_eq!(dp.get_string_date_day(),"26");
    assert_eq!(dp.get_string_date_month(),"August");
    assert_eq!(dp.has_next(),true);
    dp.next();
    assert_eq!(dp.get_string_date_day(),"27");
    dp.next();
    assert_eq!(dp.get_string_date_day(),"28");
    dp.next();
    assert_eq!(dp.get_string_date_day(),"29");
    dp.next();
    assert_eq!(dp.get_string_date_day(),"30");
    dp.next();
    assert_eq!(dp.get_string_date_day(),"31");
    dp.next();
    assert_eq!(dp.get_string_date_day(),"1");
    dp.next();
    assert_eq!(dp.get_string_date_day(),"2");
    assert_eq!(dp.get_string_numer_of_normal_year_week(),"36");
    assert_eq!(dp.get_string_numer_of_school_year_week(),"-1");
}

