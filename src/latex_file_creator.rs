use std::{env, fs, io};
use crate::date_processor::DateProcessor;

// LaTeX template to build finished document
pub const CALENDAR_TEMPLATE_TEX : &str = "Kalender_Template.tex";
// finished LaTeX document
pub const CALENDAR_TEX: &str  = "Kalender.tex";
// files to clean after LaTeX build
pub const CALENDAR_AUX : &str = "Kalender.aux";
pub const CALENDAR_LOG : &str  = "Kalender.log";

// marker to separate LaTeX document start -> \begin{document}
const START_HEADER : &str = "%%%start_header%%%";
const END_HEADER : &str = "%%%end_header%%%";

// marker to separate LaTeX \begin{document} -> page_1
const START_DOCUMENT_HEADER : &str = "%%%start_document_header%%%";
const END_DOCUMENT_HEADER : &str = "%%%end_document_header%%%";

// marker to separate page_1
const START_PAGE_1 : &str = "%%%start_page_1%%%";
const END_PAGE_1 : &str = "%%%end_page_1%%%";

// marker to separate page_2
const START_PAGE_2 : &str = "%%%start_page_2%%%";
const END_PAGE_2 : &str = "%%%end_page_2%%%";

// marker to separate document end
const START_DOCUMENT_END : &str = "%%%start_document_end%%%";
const END_DOCUMENT_END : &str = "%%%end_document_end%%%";

// marker for running elements in document
const MARKER_YEAR: &str = "@year";
const MARKER_NUMBER_OF_SCHOOL_WEEK: &str = "@numOfSchoolWeek";
const MARKER_NUMBER_OF_YEAR_WEEK: &str = "@numOfYearWeek";

const MARKER_DAY_MONDAY: &str = "@mo";
const MARKER_DAY_TUESDAY: &str = "@tu";
const MARKER_DAY_WEDNESDAY: &str = "@we";
const MARKER_DAY_THURSDAY: &str = "@th";
const MARKER_DAY_FRIDAY: &str = "@fr";
const MARKER_DAY_SATURDAY: &str = "@sa";
const MARKER_DAY_SUNDAY: &str = "@su";

const MARKER_DAY_MONDAY_MONTH: &str = "@dmoM";
const MARKER_DAY_TUESDAY_MONTH: &str = "@dtuM";
const MARKER_DAY_WEDNESDAY_MONTH: &str = "@dweM";
const MARKER_DAY_THURSDAY_MONTH: &str = "@dthM";
const MARKER_DAY_FRIDAY_MONTH: &str = "@dfrM";
const MARKER_DAY_SATURDAY_MONTH: &str = "@dsaM";
const MARKER_DAY_SUNDAY_MONTH: &str = "@dsuM";

/*
    remove line break from a string
 */
fn remove_linebreak(string: &mut String) {
    if string.ends_with('\n') {
        string.pop();
        if string.ends_with('\r') {
            string.pop();
        }
    }
}

/*
    read a string from console until it can be parsed to an i32
 */
pub fn read_year_from_console() -> i32 {
    let args: Vec<String> = env::args().collect();
    let mut input = args[0].clone();
    while let Err(_err)   = input.parse::<i32>()  {
        println!("Bitte die gewünschte Jahreszahl eingeben oder Abbruch mit Strg+C");
        input.clear();
        io::stdin().
            read_line (&mut input).
            expect("Fehler beim Lesen von der Konsole");
        // remove line break at and end of String
        remove_linebreak(&mut input)
    }
    input.parse().unwrap()
}

/*
    read template LaTeX - file -> String
    ask for a path, until input String is Ok
 */
pub fn read_template_from_path() -> String {
    let mut path : String = String::from(CALENDAR_TEMPLATE_TEX);
    while let Err(_err)  = fs::read_to_string(&path)  {
        println!("Bitte Pfad zum LaTeX Template eingeben oder Abbruch mit Strg+C");
        path.clear();
        io::stdin().
            read_line (&mut path).
            expect("Fehler beim Lesen von der Konsole");
        // remove line break at and end of String
        remove_linebreak(&mut path);
    }
    fs::read_to_string(&path).unwrap()
}

/*
    creates file CALENDAR_TEX from file CALENDAR_TEMPLATE_TEX
    for a given year
 */
pub struct LaTeXFileCreator {
    template: String,
    output: String,
    dp: DateProcessor,
}

/*
    public methods
 */
impl LaTeXFileCreator {
    pub fn new(_template: String, _year: i32) -> Box<LaTeXFileCreator> {
        Box::new(LaTeXFileCreator {
            template: _template,
            output: String::new(),
            dp: DateProcessor::new(_year),
        })
    }

    pub fn process(&mut self) {
        self.write_header();
        self.write_document_header();
        while self.dp.has_next() {
            self.write_page_1();
            self.write_page_2();
        }
        self.write_document_foot();
    }
    #[allow(dead_code)]
    pub fn print(&self) {
        println!("{}", &self.output);
    }

    pub fn write(&self) {
        fs::write(CALENDAR_TEX, &self.output).unwrap();//unwrap_or(println!("Could not create file {}", CALENDAR_TEX));
    }
}

/*
    private methods
 */
impl LaTeXFileCreator {
    fn read_part(&mut self, start_marker: &str, end_marker: &str) -> String {
        let mut inside = false;
        let mut part = String::new();
        for line in self.template.lines() {
            if line.contains(start_marker) {
                inside = true;
            } else if line.contains(end_marker) {
                inside = false;
                part.push_str(line);
                part.push_str("\n");

            }
            if inside {
                part.push_str(line);
                part.push_str("\n");
            }
        }
        part
    }

    fn write_header(&mut self) {
        println!("processing header");
        // write header
        self.output.push_str("%%%- ");
        self.output.push_str(&chrono::offset::Local::now().to_string());
        self.output.push_str(" -%%%");
        self.output.push_str("\n");
        let part = self.
            read_part(START_HEADER,END_HEADER).
            replace(MARKER_YEAR,&self.dp.get_string_year());
        self.output.push_str(&part);
    }

    fn write_document_header(&mut self) {
        println!("processing document header");
        let part = self.
            read_part(START_DOCUMENT_HEADER,END_DOCUMENT_HEADER).
            replace(MARKER_YEAR,&self.dp.get_string_year());
        self.output.push_str(&part);
    }

    fn write_page_1(&mut self) {
        println!("processing page 1 schoolweek: {}", &self.dp.get_string_numer_of_school_year_week());
        let page_1 = self.
            read_part(START_PAGE_1, END_PAGE_1).
            replace(MARKER_NUMBER_OF_SCHOOL_WEEK, &self.dp.get_string_numer_of_school_year_week()).
            replace(MARKER_YEAR, &self.dp.get_string_year()).
            replace(MARKER_NUMBER_OF_YEAR_WEEK, &self.dp.get_string_numer_of_normal_year_week());
        self.output.push_str(&page_1);
    }

    fn write_page_2(&mut self) {
        println!("processing page 2 schoolweek: {}", &self.dp.get_string_numer_of_school_year_week());
        let mut page_2 = self.read_part(START_PAGE_2, END_PAGE_2).
            replace(MARKER_NUMBER_OF_SCHOOL_WEEK, &self.dp.get_string_numer_of_school_year_week()).
            replace(MARKER_YEAR, &self.dp.get_string_year()).
            replace(MARKER_NUMBER_OF_YEAR_WEEK, &self.dp.get_string_numer_of_normal_year_week());
        page_2 = page_2.replace(MARKER_DAY_MONDAY, &self.dp.get_string_date_day()).
            replace(MARKER_DAY_MONDAY_MONTH, &self.dp.get_string_date_month());
        self.dp.next();
        page_2 = page_2.replace(MARKER_DAY_TUESDAY, &self.dp.get_string_date_day()).
            replace(MARKER_DAY_TUESDAY_MONTH, &self.dp.get_string_date_month());
        self.dp.next();
        page_2 = page_2.replace(MARKER_DAY_WEDNESDAY, &self.dp.get_string_date_day()).
            replace(MARKER_DAY_WEDNESDAY_MONTH, &self.dp.get_string_date_month());
        self.dp.next();
        page_2 = page_2.replace(MARKER_DAY_THURSDAY, &self.dp.get_string_date_day()).
            replace(MARKER_DAY_THURSDAY_MONTH, &self.dp.get_string_date_month());
        self.dp.next();
        page_2 = page_2.replace(MARKER_DAY_FRIDAY, &self.dp.get_string_date_day()).
            replace(MARKER_DAY_FRIDAY_MONTH, &self.dp.get_string_date_month());
        self.dp.next();
        page_2 = page_2.replace(MARKER_DAY_SATURDAY, &self.dp.get_string_date_day()).
            replace(MARKER_DAY_SATURDAY_MONTH, &self.dp.get_string_date_month());
        self.dp.next();
        page_2 = page_2.replace(MARKER_DAY_SUNDAY, &self.dp.get_string_date_day()).
            replace(MARKER_DAY_SUNDAY_MONTH, &self.dp.get_string_date_month());
        self.dp.next();
        self.output.push_str(&page_2);
    }

    fn write_document_foot(&mut self) {
        println!("processing document end");
        let part = self.read_part(START_DOCUMENT_END,END_DOCUMENT_END);
        self.output.push_str(&part);
    }

}
